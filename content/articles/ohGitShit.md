---
title: "Oh Shit, Git!?!"
description: "Partage de vécu sur Git"
date: 2021-12-05T11:00:00+01:00
tags: ["Tips", "Avent", "Git", "OhShitGit"]
cover:
  image: "/images/5.jpg"
  responsiveImages: true
  caption: ""
  alt: "Chiffre 5 du calendrier de l'avent"
showtoc: false
tocopen: false
hidemeta: false
comments: true
disableShare: false
---

## On peut rire avec Git

Après le cadeau 🎁 d'hier sur l'explorateur de command git, je voulais continuer sur la lancée avec ce site qui recense des mises en situation rigolotes.

Qui n'a jamais hurlé dans le bureau face à un problème Git.

Et bien ici vous retrouverez surement des moments déjà vécus. Je vous rassure chaque histoire se termine bien enfin j'espère.

> Bonne lecture [Lien vers OhShitGit](https://ohshitgit.com/en)
