---
title: "Veille - Une veille pertinente"
description: "Présentation d'une veille pertinente pour l'écosysteme React"
date: 2021-12-03T12:00:00+01:00
tags: ["Tips", "Avent", "React", "Veille", "Newsletter"]
cover:
  image: "/images/3.png"
  responsiveImages: true
  caption: ""
  alt: "Chiffre 3 du calendrier de l'avent"
showtoc: false
tocopen: false
hidemeta: false
comments: true
disableShare: false
---

## Veille ReactJs

La veille est quelque chose de très important et inhérant à mon quotidien. D'ailleurs peut-être qu'une case du calendrier de l'avent sera dédiée aux outils que j'utilise pour ma veille 😉.

En attendant, je vous propose un outil très simple, la `newsletter`, mais pas n'importe laquelle:
L'excellente newsletter de `Sébastien Lorber`, c'est hebdomadaire vous aurez l'impression d'un petit cadeau 🎁 chaque semaine.

> Vous pourrez vous abonner facilement ici [Lien vers le site de Sébastien](https://www.getrevue.co/profile/sebastien-lorber?utm_campaign=Issue&utm_content=topprofilename&utm_medium=email&utm_source=React+Hebdo)

D'ailleurs vous pourrez acceéder facilement à toutes les newsletters passées.

Je ne rentre pas dans les détails, mais si votre quotidien c'est l'écosysteme React, Js, Typescript, alors go !
