---
title: "Fira code, les fonts au service du développeur"
description: "Pouvoir prévenir des erreurs grâce aux fonts et écrire du code intuitif"
date: 2021-12-13T11:00:00+01:00
tags: ["Tips", "Avent", "Accessibilité", "Fira Code", "IDE", "Fonts", "VsCode"]
cover:
  image: "/images/13.png"
  responsiveImages: true
  caption: ""
  alt: "Nombre 13 du calendrier de l'avent"
showtoc: false
tocopen: false
hidemeta: false
comments: true
disableShare: false
---

Qui n'a jamais écrit `=!` au lieu de `!=` ou encore `=<` au lieu de `<=` et ne s'en rendre compte que bien plus tard ?
Je vous propose une solution simple.

> [Fira code : programming ligatures](https://github.com/tonsky/FiraCode)

Vous n'avez qu'à installer sur votre machine les fonts. [Installation des fonts](https://github.com/tonsky/FiraCode/wiki/Installing)

Activez ensuite la fonctionnalité de `ligature` sur votre IDE préféré par exemple sur vsCode. [Feature ligature sous vscode](https://github.com/tonsky/FiraCode/wiki/VS-Code-Instructions)

Maintenant essayez et en un coup d'oeil vous verrez si vous avez bien saisi les bons caractères pour votre comparaison : plus de faute de frappe possible.

Vous trouverez ci-dessous l'image qui montre la différence de rendu avec ou sans la ligature pour certains caractères. Le reste disponible sur le repo git [Repo git de Fira code](https://github.com/tonsky/FiraCode).

{{< figure alt="exemple rendu avec ou sans ligature" src="/images/exempla-ligatures.png">}}
