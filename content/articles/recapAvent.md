---
title: "Recap - Calendrier de l'Avent 2021 🎁🎄🎅🤶"
description: "Récapitulatif des articles de ce mois de décembre"
date: 2021-12-24T11:00:00+01:00
tags: ["Tips", "Avent", "2021", "Sommaire", "Rétrospective"]
cover:
  image: "/images/advent-calendar.png"
  responsiveImages: true
  caption: ""
  alt: "Un calendrier de l'avent"
showtoc: false
tocopen: false
hidemeta: false
comments: true
disableShare: false
---

## 24 Décembre

On est le 24, et j'ai réussi mon challenge du calendrier de l'Avent 🙌 🙌.
Aujourd'hui pas d'article sur une techno ou d'astuce, je vous propose de revenir sur ce mois de décembre avec une petite retro et un recap de ce que je vous ai partagé.
Voici pour commencer la liste des articles classés par thèmes.

### Liste des articles sur l'accessibilité :

- [Are my colors accessible]({{< ref "articles/aremycolorsaccessible.md" >}})
- [Laura Wacrenier et sa conférence sur les interfaces adaptées au daltonisme]({{< ref "articles/daltonisme.md" >}})
- [Opquast - Une formation pour l'accessibilité]({{< ref "articles/formationOpquast.md" >}})
- [Automatiser l'audit de son site]({{< ref "articles/lighthouse.md" >}})
- [Thibaud Courtoison et sa conférence : The Sound of Silence]({{< ref "articles/soundOfSilence.md" >}})
- [Reconnaissance vocale où comment rendre vos slides accessibles]({{< ref "articles/speechRecognition.md" >}})
- [L'art de l'innaccessibilité - userinyerface]({{< ref "articles/userinyerface.md" >}})

### Liste des articles sur Git :

- [Quelle commande Git lancer]({{< ref "articles/gitCommandExplorer.md" >}})
- [Oh Shit, Git!?!]({{< ref "articles/ohGitShit.md" >}})
- [Comment gérer ses hotfix avec git-worktree ?]({{< ref "articles/onStepCloserGit.md" >}})

### Liste des articles Front :

- [React devtools - Lutter contre les rendus superflus]({{< ref "articles/highlightingReactComponents.md" >}})
- [Modifier un site avec les devtools de chrome]({{< ref "articles/overridesDevtools.md" >}})
- [Bien commencer dans l'écosysteme React]({{< ref "articles/reactInit.md" >}})

### Liste des articles Devops :

- [Tester ses pipelines en local]({{< ref "articles/gitlabRunner.md" >}})
- [Les Saas aux services des projets persos]({{< ref "articles/saas.md" >}})

### La veille, les trucs et astuces by Me :

- [Veille - Une veille pertinente]({{< ref "articles/newsletterReact.md" >}})
- [Fira code, les fonts au service du développeur]({{< ref "articles/firaCode.md" >}})
- [Password rules are bullshit]({{< ref "articles/passwordIsBullshit.md" >}})
- [Maitriser son IDE - les raccourcis]({{< ref "articles/raccourcisClavier.md" >}})
- [Shields IO - la solution pour un repo plus joyeux]({{< ref "articles/shieldio.md" >}})
- [Les standards du web]({{< ref "articles/standardWeb.md" >}})
- [Comment je fais ma veille]({{< ref "articles/veille.md" >}})
- [Mes extensions VsCode]({{< ref "articles/vscodeExtensions.md" >}})

## Rétrospective

Le dernier weekend de novembre, je me décide à faire ce calendrier de l'Avent. Je ne savais pas trop dans quoi je m'embarquais.
J'avais d'ailleurs sous estimé le temps et l'effort nécessaires à le faire : bravo à tous ceux qui proposent du contenu régulièrement tout au long de l'année 👏👏.

> Petit tips : avoir cette deadline d'un article par jour m'a vraiment aidé à écrire.

J'ai eu énormement de retours sur les articles et ça fait vraiment plaisir, merci à tout ceux qui m'ont écrit, retweet, like : ça fait chaud au coeur 🥰. J'ai d'ailleurs pu échanger avec pas mal d'entre vous 💪.

Je remercie particulièrement ma chérie ❤️, qui m'a aidé pour la relecture, sans elle je n'aurais pas réussi à proposer cette qualité sur les articles. Merci à elle de m'avoir supporté chaque jour 😂.

> Qu'est ce que j'en retire ?

Plein d'idées d'articles et de sujets pour l'année qui arrive ! Bien-sûr pas au même rythme, mais je vais essayer d'écrire un article plus régulièrement sur des thèmes que j'aimerais approfondir.

Je fais beaucoup moins de fautes désormais, l'orthographe n'a jamais été mon fort, mais avec des efforts je me suis amélioré.

Désormais, j'arrive à mieux organiser mes idées : chercher à vulgariser une idée et la rendre accessible pour le plus grand nombre est un exercice très intéressant. Pour mieux m'approprier les sujets, j'ai du travailler sur la tournure des phrases, trouver d'autres mots et expliquer des termes ou des concepts : tout cela a été formateur. Je ne me suis jamais senti autant à l'aise sur les sujets que j'ai abordé que maintenant.

J'ai énormément appris, par exemple, avant de vous partager un article sur les runner gitlab - [Tester ses pipelines en local]({{< ref "articles/gitlabRunner.md" >}}) - j'ai testé sur mon ordi, j'ai du monter en compétence. L'idée de cet article m'est d'ailleurs venue en mettant en place cet outil pour répondre à un de mes propres besoins. Pourquoi garder pour soi un truc que vous trouvez génial ?

> Est-ce que je le referai l'année prochaine ? Je ne pense pas, l'exercice était intéressant mais si je dois partager du contenu à nouveau ça serait sous un autre format. #Teaser : vous me retrouverez sans doute bientôt 😉.

## Fin

En ce 24 décembre, je voulais souhaiter un joyeux Réveillon à tous ceux qui passent sur cet article.
Merci pour votre temps lors de la lecture de mon blog. Gardez votre curiosité, et n'hésitez pas à poser des questions. C'est un peu cliché, mais j'aime bien dire qu'il n'y a pas de question bête.
