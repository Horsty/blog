---
title: "Storybook - interface utilisateur à l'épreuve des balles"
description: "Comment et pourquoi j'ai wrappé une api rest avec GraphQl"
date: 2020-10-26T22:26:02+01:00
weight: 2
aliases: ["/storybook"]
tags: ["storybook"]
draft: true
cover:
  image: "/images/storybook_logo.png"
---