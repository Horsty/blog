---
title: "Wrapper une Api Rest avec GraphQl"
description: "Comment et pourquoi j'ai wrappé une api rest avec GraphQl"
date: 2020-10-26T22:26:02+01:00
draft: true
---

> Geek

```
Une personne passionnée par un ou plusieurs domaines précis, comme le cinéma, la bande dessinée, les jeux vidéo, les jeux de rôles, ou encore aux sciences, à la technologie et l'informatique.
```

> Html

```
Le HyperText Markup Language, généralement abrégé HTML, est le langage de balisage conçu pour représenter les pages web.
```

> Js

```
Abréviation de Javascript
```

> Javascript

```
Souvent abrégé en « JS » est un langage de script léger, orienté objet, principalement connu comme le langage de script des pages web.
```

> Stores

```
C'est un magasin d'application, une plateforme en ligne d'applications accessible depuis un système compatible
```

> Application Hybrid

```
C'est une application utilisant le navigateur web intégré du support (Smartphone ou tablette) et les technologies Web (HTML, CSS et Javascript) pour fonctionner sur différents OS (iOS, Android, Windows Phone, etc.).
```

> Application native

```
C'est une application mobile qui est développée spécifiquement pour un des systèmes d'exploitation, un OS,  utilisé par les smartphones et tablettes (iOS, Android, etc.).
```

> OS

```
de l'anglais Opérating System, c'est l'abréviation de Systèeme d'exploitation
```

> Système d'exploitation

```
est un ensemble de programmes qui dirige l'utilisation des ressources d'un ordinateur par des logiciels applicatifs
```

> Responsive

```
C'est une approche de conception Web dans le but d'offrir une expérience de lecture et de navigation optimales pour l'utilisateur quelle que soit l'appareil utilisé (smartphone, tablettes, liseuses, écran d'ordinateur).
Cela se traduit par exemple par du redimensionnement, du recadrage ou encore du défilements multidirectionnels de pages.
```

> UX

```
Fait référence à l'expérience utilisateur, de l'anglais, user experience, c'est la qualité du vécu de l'utilisateur dans des environnements numériques ou physiques. C'est une notion proche qui est proche des principes d'ergonomie des logiciels et d'utilisabilité.
```

> UI

```
Pour interface utilisateur, de l'anglais, user interface, est un dispositif matériel ou logiciel qui permet à un utilisateur d'interagir avec un produit informatique.
```

> PWA

```
Pour progressive web app (applications web progressive en français) est une application web qui consiste en des pages ou des sites web, et qui peuvent apparaître à l'utilisateur de la même manière que les applications natives ou les applications mobiles.
```

> Build

```
C'est à la fois l'action de compiler un programme et son résultat, une version exécutable.
```

> URL

```
Pour Uniform Resource Locator, couramment appelée adresse web, est une chaîne de caractères qui permet d'identifier une ressource en ligne par son emplacement et de préciser le protocole internet pour la récupérer (par exemple http ou https).
```

> hardware

```
Désigne la partie physique des appareils, des dispositifs de télécommunication, de stockage, et des périphériques en général.
```

> notifications push

```
C'est un message d'alerte envoyé à l'utilisateur et qui s'affiche sur son écran.
```

> Service Workers

```
Ils permettent la création d'expériences de navigation déconnectée efficaces, en interceptant les requêtes réseau et en effectuant des actions appropriées selon que le réseau est disponible et que des ressources mises à jour sont à disposition sur le serveur.
```

> navigateur

```
C'est un logiciel conçu pour consulter et afficher le web. C'est au minimum un client HTTP.
```

> HTTP

```
Pour Hypertext Transfer Protocol est un protocole de communication client-serveur développé pour le web. HTTPS (avec S pour secured, soit « sécurisé ») est la variante sécurisée par l'usage des protocoles Transport Layer Security (TLS).
```

> SEO

```
Pour Search Engine Optimization en anglais c'est l'optimisation pour les moteurs de recherche, appelé également référencement naturel ou organique en français. Cela inclut l'ensemble des techniques qui visent à améliorer le positionnement d'une page, d'un site ou d'une application web dans la page des résultats de recherche d'un moteur de recherche.
```

> Référencement

```
Cela consiste à améliorer le positionnement et la visibilité de sites dans des pages de résultats de moteurs de recherche ou d'annuaires.
```

> Framework

```
Désigne un ensemble cohérent de composants logiciels structurels, qui sert à créer les fondations ainsi que les grandes lignes de tout ou d’une partie d'un logiciel (architecture).
```

> Open source

```
C'est une désignation qui s'applique aux logiciels dont la licence respecte des critères précisément établis par l'[Open Source Initiative](https://opensource.org/), c'est-à-dire les possibilités de libre redistribution, d'accès au code source et de création de travaux dérivés. Mis à la disposition du grand public, ce code source est généralement le résultat d'une collaboration entre programmeurs.
```

> wiki

```
Un wiki est une application web qui permet la création, la modification et l'illustration collaboratives de pages à l'intérieur d'un site web.
```

> Répertoire

```
Un répertoire ou un dossier est une liste de descriptions de fichiers. Du point de vue du système de fichiers, il est traité comme un fichier dont le contenu est la liste des fichiers référencés.
```

> Template

```
Un template connu également sous le terme de modèle, layout ou Gabarit en français, représente l’ensemble des éléments graphiques de l’agencement des colonnes, passant par le choix des couleurs jusqu’à l’établissement de la structure des différents éléments enveloppant un site Internet, abstraction faite de son contenu.
```

> Css

```
De l'anglais Cascading Style Sheets, ces feuilles de style en cascade, forment un langage informatique qui décrit la présentation des documents HTML et XML.
```

> Hyperlien

```
Un hyperlien ou lien hypertexte, est une référence dans un système hypertexte permettant de passer automatiquement d'un document consulté à un autre document.
```

> Markdown

```
Markdown est un langage de balisage léger
```

> Yaml

```
YAML, acronyme récursif de YAML Ain't Markup Language (« YAML n’est pas un langage de balisage ») , est un format de représentation de données
```

> Feedback

```
Retour sur une expérience,
```

> flux RSS

```
Venant de l'anglais really simple syndication, est une famille de formats de données utilisés lors de la diffusion du contenu d'un site sous forme de flux.
```

> desktop

```
Bureau d'un environnement graphique, c'est l'écran sur lequel apparaissent des icônes et des fenêtres, l'arrière-plan est souvent agrémenté d'une image.
```

> API

```
Une interface de programmation d’application ou interface de programmation applicative, Application Programming Interface en anglais, est un ensemble normalisé de classes, de méthodes, de fonctions et de constantes qui sert de façade par laquelle un logiciel offre des services à d'autres logiciels.
```

> Marketplace

```
Est une plateforme permettant de mettre en relation des clients auprès de nombreux fournisseurs, qui peuvent être des particuliers.
```

> json

```
JavaScript Object Notation est un format de données textuelles dérivé de la notation des objets du langage JavaScript. Il permet de représenter de l’information structurée.
```

> ID

```
IDentifier, abréviation anglaise de "Identification" est l'intitulé d'un code unique permettant de désigner précisément une ligne de données dans une liste, table informatique ou base de données électroniques.
```

> GET

```
GET, une méthode pour passer des variables à un serveur web via l'URL.
```

> POST

```
Dans le protocole HTTP, POST est une méthode de requête pour les pages web.
```

> Client web

```
un client est le logiciel qui envoie des demandes à un serveur. Il peut s'agir d'un logiciel manipulé par une personne, ou d'un robot.
```

> serveur web

```
Un serveur web est, soit un logiciel de service de ressources web (serveur HTTP), soit un serveur informatique (ordinateur) qui répond à des requêtes du web sur un réseau public ou privé en utilisant principalement le protocole HTTP.
```

> Requête

```
Est un moyen formel d'effectuer une recherche d'information dans un système d'information. Cela peut être un message envoyé par un client vers un serveur qui émet une réponse ou une interrogation à une base de donnée. Le client reçoit comme réponse des éléments de cette base correspondant aux critères définis dans la requête.
```

> Models
```
Un modèle a pour objectif de structurer les informations et activités d'une organisation : données, traitements, et flux d'informations entre entités.
```

> Resolver

> Mapping
```
Est la mise en cohérence entre deux types d'informations distincts, deux données.
```

> Bdd
```
Abréviation de base de données (en anglais database), permet de stocker et de retrouver des données brutes ou de l'information.
```

> Concaténation
```
Désigne l'action de mettre bout à bout au moins deux chaînes de caractères ou deux résultats.
```

> Front-end
```
Le développement web frontal (aussi appelé front-end en anglais) correspond aux productions HTML, CSS et JavaScript d’une page internet ou d’une application qu’un utilisateur peut voir et avec lesquelles il peut interagir directement.
```

> Backend
```
Est un terme désignant une partie d'un logiciel devant produire un résultat. On l'oppose au front-end qui lui est la partie visible pour l'utilisateur.
```

> PO
```
Product Owner, « propriétaire du produit » est la personne ayant la responsabilité de produire et de maintenir à jour le carnet de produit. C'est lui qui détermine les priorités et qui prend les décisions d'orientation du projet.
```

> Software craftmanship
```
Le Software craftsmanship ou l'« artisanat du logiciel » est une approche de développement de logiciels qui met l'accent sur les compétences de codage des développeurs.
```

> Poker planing
```
Le planning poker est une façon ludique de produire des estimations sur l'effort de développement de fonctionnalités.
```

> US
```
Un récit utilisateur, ou « user story » en anglais, est une description simple d’un besoin ou d’une attente exprimée par un utilisateur et utilisée pour déterminer les fonctionnalités à développer.
```

> ESN
```
Une entreprise de services du numérique (ESN), anciennement société de services en ingénierie informatique (anciennement SSII ou SS2I), est une société de services experte dans le domaine des nouvelles technologies et de l’informatique.
```

> Php
```
Php pour Hypertext Preprocessor, est un langage de programmation libre, principalement utilisé pour produire des pages Web dynamiques via un serveur HTTP, mais pouvant également fonctionner comme n'importe quel langage interprété de façon locale. PHP est un langage impératif orienté objet.
```

> Devops
```
Le DevOps est un mouvement en ingénierie informatique et une pratique technique visant à l'unification du développement logiciel (dev) et de l'administration des infrastructures informatiques (ops), notamment l'administration système.
```
