---
draft: true
---
Dans notre métier, il arrive souvent d'être confronté à des soucis de `fuite mémoire` ou `memory leak`.
Un des premiers signes est souvent un pc qui ralenti. On peut le voir très vite avec votre gestionnaire de tache et vous verrez que votre navigateur aspire litérallement toute votre RAM.
Mais concretement comment identifier précisément ce qui cause ces fuite mémoire.

Visualiser l'usage de la mémoire au travers le temps avec un enregistrement.
Identifier les arbres de DOM détachés avec HEAP Snapshot
Trouver le moment ou de la mémoire est alloué à votre JS Heap
Identify detached DOM trees (a common cause of memory leaks) with Heap Snapshots.
Find out when new memory is being allocated in your JS heap with Allocation Timeline recordings.