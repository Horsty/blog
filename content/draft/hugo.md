---
title: "Hugo - créer son blog"
description: "Pourquoi j'ai lancé mon blog avec Hugo"
date: 2020-10-26T22:26:02+01:00
weight: 1
draft: true
aliases: ["/hugo"]
tags:
  [
    "hugo",
    "wordpress",
    "go",
    "open-source",
    "soft-skill",
    "confluence",
    "wiki",
    "markdown",
  ]
cover:
  image: "/images/hugo-logo.png"
---

# Hugo c'est qui ?

Alors Hugo ce n'est pas ce bogoss qui vient prendre l'apéro le jeudi soir, non non. Hugo c'est un framework qui permet de créer des sites web statiques très rapidement. C'est open-source :) et c'est écrit en Go. Pas besoin d'en dire plus.

# Pourquoi Hugo ?

Un de mes traits de caractère (à vous de juger si c'est un défaut ou une qualité) c'est que je suis extrêmement curieux et j'adore partager mes expériences et découvertes.

Quel meilleur moyen pour partager ce que j'ai envi qu'un blog ?

Et plutôt que de partir dans une usine à gaz comme Wordpress, j'ai voulu utiliser quelque chose de simple.

J'avais déjà utilisé Hugo dans un précédent projet, qui nous servait de wiki un peu comme confluence (proposé par Atlasian).  Je connaissais déjà l'utilité du framework.

Autre gros avantage à Hugo, c'est open-source, et la communauté a créé une floppé de thèmes, et comme je ne veux pas perdre de temps sur le design, j'ai juste eu à chercher un thème simple qui me convenait :[Hugo-Papermod](https://github.com/adityatelange/hugo-PaperMod)

# Comment ça marche ?

Hugo repose sur un fichier principal `config.yml` qui permet, comme son nom le laisse à supposer, de configurer le site généré. C'est dans ce fichier que l'on configure le thème à utiliser.

En plus de ce fichier, Hugo se base sur une structure de répertoire pour fonctionner.

```
.
├── archetypes
├── config.yml
├── content
├── data
├── layouts
├── static
└── themes
```

On trouve un dossier `themes` justement, avec tout le template, le css, etc.

Un dossier static avec les ressources utilisées. Dans mon cas des images ou des gifs pour mes super articles.

Un dossier `content` qui contiendra nos articles. A savoir que les noms des fichiers et répertoire ont leurs importances pour les liens hypertextes par exemple.

Il existe d'autres dossiers que je ne détaille pas, je vous laisse la doc sur la structure pour plus d'informations : [Hugo directory structure](https://gohugo.io/getting-started/directory-structure/)

Ensuite rien de plus simple, c'est du Markdown pour la rédaction, et du yaml pour la config, du html / css pour la partie design.

# Et les outils de blog ?

Hugo fonctionne très simplement avec les solutions tierces faciles à mettre en place.
Il faut avant tout savoir ce que l'on veut. Pour ma part, je veux un blog où :

- Je peux diffuser du contenu, avoir du feedback sur les articles via des commentaires - [Intégration de Disqus par Hugo](https://gohugo.io/content-management/comments/),
- Je laisse à l'utilisateur la possibilité de m'écrire en dehors des réseaux sociaux - [Formspree - gestionaire de formulaire](https://formspree.io/),
- Je met à disposition un flux RSS - [Hugo - Template RSS](https://gohugo.io/templates/rss/).

Si je souhaite plus de fonctionnalités, je choisirai wordpress.
Tout les outils, intégrations que propose Hugo se trouve très facilement sur leur documentation, donc encore une fois, je vous laisse chercher.

Ah !petit truc en plus : Hugo permet l'intégration facile de google Analytics, si jamais vous voulez traquer comme un fou ! [Hugo - Google Analytics](https://gohugo.io/templates/internal#google-analytics)

# La force d'Hugo ?

La force du framework Hugo réside en plusieurs points :

- C'est un projet `Opensource`, avec tous les avantages que cela procure. La communauté est assez importante, une réponse reste rarement sans réponse.[Plateforme Communautaire Hugo](https://discourse.gohugo.io/)
- Une multitude de `template`, vous trouverez le votre facilement [Theme Hugo](https://themes.gohugo.io/)
- La `simplicité`, pas besoin de connaitre les spécificités d'un langage particulier, ici on ne fait que du html et css, et encore, seulement si vous faites votre thème, autrement ce n'est que de la rédaction d'articles en markdown et de la configuration dans un fichier yaml.

Tout ça pour un gain de temps et une rapidité de mise en ligne.
J'ai passé plus de temps à chercher le template idéal qu'à le mettre en ligne.
Mon projet est versionné sur gitlab, le tout branché sur Netlify, comme ça à chaque merge dans Master j'ai un build automatique et mon blog est à jour.
Je verrai plus tard pour automatiser cela sur mon propre serveur mais "ça fait le taf" et c'est tout ce que je souhaite pour l'instant.

# Pour résumer
Hugo c'est simple, tant que vous voulez quelque chose de simple. On ne va pas lui demander la lune.
Si vous avez besoin de quelque chose, la doc est là pour aider.
[Doc Hugo](https://gohugo.io/documentation/)

# Le mot de la fin
J'espère qu'avec cette article très simple je vous aurais donné envie de vous lancer dans l'écriture de votre propre blog ou dans la réalisation d'un wiki pour votre équipe :)
