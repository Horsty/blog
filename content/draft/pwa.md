---
title: "PWA - état des lieux"
description: "Petite explication autour des Progressive Web App"
date: 2020-11-10
weight: 1
draft: true
aliases: ["/pwa"]
tags: ["pwa", "react", "application mobile"]
cover:
  image: "/images/pwa-logo.png"
---

# Préambule

Je suis développeur web et je passe du temps sur des projets (perso ?). C'est d'ailleurs au travers de mon expérience acquise lors du développement d'un de ces projets que je vais vous présenter les Progressive Web App.

J'utilise principalement les technos que je maîtrise, pour des questions de temps et de facilité. Eh oui ! Pourquoi se "prendre la tête" sur une nouvelle techno alors que je peux faire la même chose en HTML et Javascript. Cependant, les modes de consommation des utilisateurs évoluent, le mobile est un contexte d'utilisation de plus en plus important. Mon objectif est donc de permettre l'accessibilité de mon projet sur mobile tout en travaillant sur des technologies web.

Mais alors comment proposer mon application web sur mobile ?

Refaire une application pour IOS, une autre pour Android et les publier sur les stores ? Cette solution peut être coûteuse.
Par ailleurs, j'aime m'affranchir de ce genre de contraintes, je préfère rester sur le web.

Faire une application hybride : ReactNative, Ionic, Flutter, etc. ? Oui ça peut être une solution, mais ça oblige à gérer le build pour chaque plateforme, cela entraine des versions désynchronisées selon le contexte, etc.

Faire un site responsive ? Oui très bien, mais ça ne résout pas tous les problèmes. Certes, mon site aura un rendu optimisé sur mobile, mais l'expérience utilisateur - UX - ne le sera pas.

Ce que je souhaite, c'est avoir un seul projet qui, selon le contexte d'utilisation, soit optimisé.
C'est là qu'interviennent les PWA - Progressive Web App.

# Qu'est ce qu'une PWA ?

C'est une application Web, qui peut se comporter comme une application mobile "classique". C'est tout : rien de plus, rien de moins.

Plus précisément, une PWA, est accessible via une URL comme n'importe quel site web, et peut, si l'utilisateur le souhaite, s'installer sur la page d'accueil de votre mobile ou même de votre ordinateur !

{{< figure src="/images/outlook_install.jpg" >}}

#### exemple avec Outlook sous Windows

{{< figure src="/images/install-twitter.png" >}}

#### exemple avec Twitter sous Ios

{{< figure src="/images/install-starbuck.png" >}}

#### exemple avec Starbuck sous Android

Si on ne s'arrête qu'à ça, la PWA n'apporte pas grand chose. Ce n'est qu'un simple raccourci vers un site web, me direz-vous.
Alors, maintenant que vous avez installé votre application, allez dessus, vous ne remarquez rien ?
Plus de barre d'adresse, plus de menu propre au téléphone. On se croirait dans une application qui provient des stores, non ?

{{< figure src="/images/mobile-pwa.png" >}}

#### PWA twitter avant vs après l'installation

# Une PWA ça permet quoi ?

Après avoir vu ce qu'est une PWA et comment l'installer, rentrons un peu plus dans le vif du sujet. Techniquement, qu'est ce que me permet une PWA par rapport à une application native ?
Eh bien, comme c'est un site web tous les standards HTML sont disponibles : une PWA peut utiliser des composants hardware des mobiles et diverses fonctionnalités comme les notifications push. Il existe bien d'autres avantages comme la consultation et l'utilisation de notre application en mode "hors-ligne".

Comment ce qui possible en PWA ? Voici quelques adresses bien pratiques :

- [Api HTML5 - Mozilla](https://developer.mozilla.org/fr/docs/Web/Guide/HTML/HTML5) - Doc entière proposée par Mozilla
- [What Web Can Do](https://whatwebcando.today/) - checklist des standard HTML5 rapide à visualiser

Aujourd'hui on en est là et ça évolue très vite !

{{< figure src="/images/whatwebcando.today_.png">}}

# Comment ça marche ?

Bon tout ça c'est bien joli, mais concrètement comment je transforme mon site web en PWA ?
Pour la partie fonctionnelle, tout se passe sur l'implémentation de Service Workers. Ce sont des scripts qui fonctionnent en parallèle de la page web. Ils vont permettre notamment le mode hors-ligne et la mise à jour des contenus en arrière-plan.
Concernant le comportement graphique sur le mobile, il est géré via un fichier `manifest.json` qui contient notamment l'icône par défaut, le nom et d'autres options.
Je ne vais pas rentrer dans les détails des Services Workers, je réserve cela pour un autre billet de blog mais je me permets de vous laisser quelques liens :

- [Service Worker - Mozilla](https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API) - Doc entière proposée par Mozilla
- [manifest.json - Mozilla](https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/manifest.json) - Doc entière proposée par Mozilla
- [Create React App - PWA](https://create-react-app.dev/docs/making-a-progressive-web-app/) - qui est la doc en ligne pour developper votre PWA en React

# Quand utiliser une PWA ?

Maintenant que nous avons abordé tout cela, il nous reste à déterminer les cas dans lesquels une PWA constitue une solution judicieuse à un problème.

C'est là que rôle de développeur tel que je le conçois prend tout son sens. Lors de mes missions, j'ai toujours en tête qu'avant d'apporter mes compétences de développement, j'apporte avant tout des conseils et une expertise.

Lorsqu'un client souhaite à tout prix faire une application mobile, mon rôle est de l'accompagner dans son choix en lui présentant les solutions existantes et en le conseillant sur le choix le plus pertinent.

Est-ce qu'une application native est un choix pertinent ? Pour répondre à cette question, rien de plus simple grâce à un petit tableau de versus :

| Fonctionnalités                   |                          PWA                           |                                               Native |
| --------------------------------- | :----------------------------------------------------: | ---------------------------------------------------: |
| Disponible sur navigateur         |                          Oui                           |                                                  Non |
| Disponible dans le store          | Oui (google play) Non (apple store) mais ça va arriver |                                                  Oui |
| Au service du référencement (SEO) |           Oui (google met en avant les PWA)            |                                                  Non |
| Icone sur l'écran d'accueil       |                          Oui                           |                                                  Oui |
| Fonction de partage               |                          Oui                           |                                                  Oui |
| Rapidité                          |                         Rapide                         |                                          Très rapide |
| Espace de stockage                |                          Peu                           |                                             Beaucoup |
| Compétences nécessaires           |                Les mêmes qu'un site web                |            Compétence spécifique selon la plateforme |
| Mise à jour                       |      Automatique et invisible pour l'utilisateur       | Demande un passage par les stores (complexe et long) |

Je pourrai continuer la liste encore longtemps mais vous voyez l'idée. Globalement, toutes les fonctionnalités d'une application Native sont disponibles via une PWA ou le seront bientôt.
Le choix va notamment dépendre du budget disponible ou des compétences présentes dans l'équipe. Il est clairement plus simple de capitaliser sur des compétences déjà présentes que de recruter un développeur mobile.
Actuellement, le principal point de blocage réside dans la disponibilité et la visibilité de l'application sur les Stores. Google est en avance mais Apple, depuis les dernières mises à jour IOS, rattrape son retard.
Windows est clairement devant.
Dans tous les cas, faites-vous votre propre avis, testez !

# En résumé

En plus d'une implémentation technique (vu au travers des Services Workers), le développement d'une PWA ne s'arrête pas là.
Choisir entre une PWA ou une application native nécessite une réflexion en amont, pas seulement sur des aspects techniques.
J'aime à croire que "penser PWA" est un moyen de s'affranchir des contraintes établies dans le monde du mobile (AppStore par exemple). Cela nous donne également, à nous développeurs/euses, des moyens de proposer nos applications autrement, sans que cela soit au détriment de l'utilisateur. Dans certains cas, l'expérience sera même optimisée. Quel plaisir de ne plus avoir à me soucier de la mise à jour des app installées sur mon téléphone !

# Le mot de la fin

Voilà la fin de cet article qui introduit la notion de PWA. J'espère qu'il vous donnera envie d'approfondir le sujet. Si vous avez des questions, n'hésitez pas à laisser un commentaire.
