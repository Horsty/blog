![GitHub](https://img.shields.io/github/license/adityatelange/hugo-PaperMod)
[![hugo-papermod](https://img.shields.io/badge/Hugo--Themes-@PaperMod-blue)](https://themes.gohugo.io/hugo-papermod/)
[![Netlify Status](https://api.netlify.com/api/v1/badges/4f81883d-0207-406a-abd7-7cc14394fafc/deploy-status)](https://app.netlify.com/sites/horsty-blog/deploys)
[![Lighthouse report](https://img.shields.io/static/v1.svg?label=Lighthouse&message=report&color=green)](https://horsty.gitlab.io/blog/report.html)

# Ressources for this blog

- Hugo PaperMod the theme that i use [PaperMod](https://github.com/adityatelange/hugo-PaperMod)
- Article for [Canonical Url](https://blog.concannon.tech/tech-talk/hugo-canonical-url/) implementation
- Form implementation [Brianli article](https://brianli.com/how-to-create-a-contact-form-in-hugo-with-netlify-forms/)
